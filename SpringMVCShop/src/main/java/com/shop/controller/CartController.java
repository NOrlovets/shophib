package com.shop.controller;


import com.shop.DAO.CartDAO;
import com.shop.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/main/cart/")
public class CartController {

    @Autowired
    private CartDAO cartDAO;

    @GetMapping
    public List<Product> cart() {
        return cartDAO.getCart();
    }

    @PostMapping("add/{id}")
    public String addcart(@PathVariable String id) {
        cartDAO.add(id);
        return "TakeMyString";
    }

    @DeleteMapping("{id}")
    public List<Product> deletecart(@PathVariable String id) {
        cartDAO.delete(id);
        return cartDAO.getCart();
    }

}
