package com.shop.controller;


import com.shop.DAO.UserDAO;
import com.shop.domain.Category;
import com.shop.domain.User;
import com.shop.manager.UserManager;
//import com.shop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
public class RegController {

    @Autowired
    private UserDAO userDAO;

    @GetMapping("/reg")
    public String user() {
        return "reg";
    }

    @PostMapping("/reg")
    public String regPost(@Valid User user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "reg";
        }
        userDAO.create(user);
        return "redirect:/main";
    }
}
