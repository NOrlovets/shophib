<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="css/themes.css">
    <title>Auth</title>
</head>
<body>

<div class="login-page">
    <div class="form">
        <h1 class="upmessage">Authorization</h1>

        <form action="auth" method="POST">
            <input type="text" name="login" placeholder="login"/>
            <input type="password" name="password" placeholder="password"/>
            <button>login</button>
        </form>

        <p class="message"><a href="reg">Registration</a></p>
    </div>
</div>

</body>
</html>
